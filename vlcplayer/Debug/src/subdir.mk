################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/CFont.cpp \
../src/CImage.cpp \
../src/CSDL.cpp \
../src/CUtil.cpp \
../src/Player.cpp \
../src/global.cpp \
../src/list.cpp \
../src/parsing.cpp \
../src/vlcplayer.cpp 

OBJS += \
./src/CFont.o \
./src/CImage.o \
./src/CSDL.o \
./src/CUtil.o \
./src/Player.o \
./src/global.o \
./src/list.o \
./src/parsing.o \
./src/vlcplayer.o 

CPP_DEPS += \
./src/CFont.d \
./src/CImage.d \
./src/CSDL.d \
./src/CUtil.d \
./src/Player.d \
./src/global.d \
./src/list.d \
./src/parsing.d \
./src/vlcplayer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/include/SDL2 -O0 -g3 -Wall -c -fmessage-length=0 -D_REENTRANT -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


